﻿using task1.Model;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace task1.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Register(RegistrationBindingModel model)
        {
            if (model.ConsultationDate < DateTime.Now || model.ConsultationDate.DayOfWeek == DayOfWeek.Saturday ||
                model.ConsultationDate.DayOfWeek == DayOfWeek.Sunday)
            {
                ModelState.AddModelError(nameof(model.ConsultationDate), "Помилка дати. Консультація неможливо у вихідні дні або " +
                    "Ви вказуєте день, який вже пройшов");
            }

            if (model.SelectedProduct == "Основи" && model.ConsultationDate.DayOfWeek == DayOfWeek.Monday)
            {
                ModelState.AddModelError(nameof(model.SelectedProduct), "Помилка. Консультація з курсу \"Основи\" не проводиться по понеділкам");
            }

            if (ModelState.IsValid)
            {
                return View("Success", model);
            }
            else
            {
                return View("Index");
            }
        }
    }
}

