﻿using System.ComponentModel.DataAnnotations;

namespace task1.Model
{
    public class RegistrationBindingModel
    {
        [Required(ErrorMessage = "Введіть ім'я")]
        [Display(Name = "Ім'я")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Введіть прізвище")]
        [Display(Name = "Прізвище")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Введіть імейл")]
        [Display(Name = "Імейл")]
        [EmailAddress]
        public string Email { get; set; }

        [Required(ErrorMessage = "Введіть дату консультації")]
        [Display(Name = "Дата консультації")]
        public DateTime ConsultationDate { get; set; }

        [Required(ErrorMessage = "Оберіть продукт")]
        [Display(Name = "Оберіть продукт:")]
        public string SelectedProduct { get; set; }
    }
}
